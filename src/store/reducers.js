/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-06 15:32:34
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-06 22:44:16
 * @FilePath: /twitter/src/store/reducers.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// 工具函数，用于组织多个reducer，并返回reducer集合
import { combineReducers } from 'redux'
// 默认值
import defaultState from './state.js'
// 一个reducer就是一个函数
  
  function setInfoList (state = defaultState, action) {
    switch (action.type) {
      case 'ADD':
         state.infoList.unshift(action.newData)
        return state
      case 'DEL':
        let list = state.infoList.filter((val) => val.id !== action.id)
        state.infoList = list;
        return state
      default:
        return state
    }
  }
  
  export default setInfoList
  // 导出所有reducer
//   export default combineReducers({
//     //   pageTitle,
//     setInfoList
//   })