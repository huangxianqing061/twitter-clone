/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-06 15:32:47
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-08 10:35:46
 * @FilePath: /twitter/src/store/state.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// export default {
    
// }
let date = new Date()
const time = date.toLocaleString()
const defaultState ={
    infoList: [{
        id: 1,
        title: 'huangxianqing',
        content: 'Nice to meet you, I am a front-end engineer, I love development, love front-end technology, I hope to get this valuable opportunity, thank you very much',
        userName: '',
        time: time
    },{
        id: 2,
        title: 'huangxianqing',
        content: 'I am trying to learn Japanese recently, I have not used React for a long time. I do not write well. Please forgive me. I will study hard',
        userName: '',
        time: time
    }]
}
export default defaultState