import React from 'react'
import TweetBox from './TweetBox'
const MobileTweetBox = () => {
  return (
    <div>
      <TweetBox flag="mobile"/>
    </div>
  )
}

export default MobileTweetBox
