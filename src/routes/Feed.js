/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-05 10:48:03
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-07 21:34:22
 * @FilePath: /twitter/src/Feed.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React from "react";
import './Feed.css'
import TweetBox from "./TweetBox"
import Post from "./Post"
import { useViewport } from '../util/ViewportContext';
import MobileFeed from "./MobileFeed";

function Feed () {
    const { width } = useViewport();
    const breakpoint = 760;
    return (
        width > breakpoint ? ( <div className="feed">
        <div className="feed_header">
            <h2>Home</h2>
        </div>
        {localStorage.getItem('username') ? (<TweetBox  />) : ''}
        <Post />
    </div>) : <MobileFeed />
       
    )
}
export default Feed 