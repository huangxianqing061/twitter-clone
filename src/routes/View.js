/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-07 13:05:18
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-08 10:57:59
 * @FilePath: /twitter/src/View.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React from 'react'
import "./View.css"
import { Avatar } from "@material-ui/core"
import BackIcon from "@material-ui/icons/Backspace"
import qs from "querystring"
import store from '../store/index.js'
function View(props) {
  const { search } = props.location
  const { id } = qs.parse(search.slice(1))
  let state = store.getState().infoList
  let obj = state.find(item => item.id == id)
  const handleBack = () => {
    window.history.back()
  }
  return (
    <div className='view'>
      <div className='view_back'>
        <BackIcon onClick={() => {handleBack()}} />
        <h2>Tweet</h2>
      </div>
      <div className='view_header'>
        <div className='view_avatar'>
          <Avatar src="" />
        </div>
        <div className='view_headerText'>
        <h3>
                {obj.title}
              </h3>
        </div>
      </div>
      <div className='view_body'>
        <p>{obj.content}</p>
      </div>
      <div className='time'>{obj.time}</div>
    </div>
  )
}
export default View
