import React from 'react'
import "./MobileSidebar.css"
import HomeIcon from "@material-ui/icons/Home"
import SearchIcon from "@material-ui/icons/Search"
function MobileSidebar() {
  return (
    <div className='mobileSidebar'>
      <div>
        <HomeIcon/>
      </div>
      <div>
        <SearchIcon/>
      </div>
      <div>
        <HomeIcon/>
      </div>
      <div>
        <HomeIcon/>
      </div>
    </div>
  )
}

export default MobileSidebar
