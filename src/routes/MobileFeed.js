import React from 'react'
import { Avatar, Button } from "@material-ui/core"
import TwitterIcon from "@material-ui/icons/Twitter"
import AddIcon from "@material-ui/icons/Add"
import MobilePost from "./MobilePost"
import "./MobileFeed.css"
import { Link } from "react-router-dom";
function MobileFeed() {
  const handleQuit = () => {
    localStorage.clear()
    window.location.reload()
  }
  return (
    <div>
      <Avatar src="" className='mobileFeed_avatar' onClick={()=>(handleQuit())}/>
      <div className='mobileFeed_box'>
        <TwitterIcon className="mobileFeed_twitterIcon"/>
      </div>
      <MobilePost />
      {localStorage.getItem('username') ? (<Link to={"/mobileTweetBox"} style={{ textDecoration:'none'}}>
        <div>
            <AddIcon className='addIcon'/>
        </div>
      </Link>) : ''}
      
    </div>
  )
}

export default MobileFeed
