/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-03 20:33:15
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-07 20:46:34
 * @FilePath: /twitter/src/App.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { Suspense } from 'react';
import './App.css';
import Sidebar from './Sidebar';
import Feed from './Feed';
import Widgets from './Widgets';
import Footer from './Footer';
import MobileTweetBox from './MobileTweetBox';
import Explore from './Explore';
import View from './View';
import 'antd/dist/reset.css';
import { Route, Switch, Link } from "react-router-dom";
import ViewportContext from '../util/ViewportContext.js';
function App() {
  return (
    <div className="app">
      <ViewportContext>
      <Suspense>
        <Sidebar />
        <Switch>
          <Route path="/" exact component={Feed} />
          <Route path="/home" component={Feed} />
          <Route path="/explore" component={Explore} />
          <Route path="/view" component={View} />
          <Route path="/mobileTweetBox" component={MobileTweetBox} />
        </Switch>
        {/* <Widgets /> */}
        <Footer />
      </Suspense>
      </ViewportContext>
      {/* <Feed /> */}
      


    </div>
  );
}

export default App;
