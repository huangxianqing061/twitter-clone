/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-03 21:55:23
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-07 20:58:34
 * @FilePath: /twitter/src/SidebarOption.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React from "react";
import './SidebarOption.css';
import { Link } from 'react-router-dom';
function SidebarOption({ active, text, Icon, linkUrl }) {
    return (
        <Link to={linkUrl} style={{ textDecoration:'none'}}>
            <div className={`sidebarOption ${active && 'sidebarOption--active'}`}>
                <Icon />
                <h2>{text}</h2>
            </div>
        </Link>

    )
}
export default SidebarOption; 