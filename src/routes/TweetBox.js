/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-05 12:00:50
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-08 13:38:40
 * @FilePath: /twitter/src/TweetBox.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React from "react";
import './TweetBox.css';
import {  Form, Input } from 'antd';
import { Avatar, Button } from "@material-ui/core"
// import { Store } from "@material-ui/icons";
import Store from "../store/index"
const { TextArea } = Input;
function TweetBox (props) {
    const form = React.createRef();
    let date = new Date()
    const time = date.toLocaleString()
    const handleClick = () => {
        const newData = {
            id: Store.getState().infoList.length + 1,
            title: localStorage.getItem('username'),
            content: form.current.getFieldsValue().input,
            userName: localStorage.getItem('username'),
            time: time
        }
        const addData = {
            type: "ADD",
            newData
        }
        Store.dispatch(addData)
        form.current.resetFields()
        if (props.flag == "mobile") {
            window.history.back()
        }
    };
    // 发布btn样式
    return (
        <div className="tweetBox">
            <form>
                <div className="tweetBox_input"> 
                    <Avatar src="" />
                    <Form
                        ref={form}
                        name="basic"
                    >
                        <Form.Item
                            name="input"
                        >
                             <TextArea rows={2} placeholder="What's happening" className="tweetBox_input_box"/>
                            {/* <Input  className="tweetBox_input_box"/> */}
                        </Form.Item>
                    </Form>
                    {/* <input placeholder="What's happening" type="text"></input> */}
                </div>
                <Button className="tweetBox_btn" onClick={handleClick}>Tweet</Button>
            </form>
        </div>
    )
}
export default TweetBox 