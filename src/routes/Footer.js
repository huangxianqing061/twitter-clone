/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-06 08:19:52
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-08 13:46:19
 * @FilePath: /twitter-clone/src/routes/Footer.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { Button } from '@material-ui/core'
import React from 'react'
import { Checkbox, Form, Input, Modal } from 'antd';
import "./Footer.css"
function Footer() {
    let isShowFooter = localStorage.getItem('username')
    const [open, setOpen] = React.useState(false);
    const form = React.createRef();
    //   const { register, handleSubmit, errors } = useForm()
    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleOk = () => {
        localStorage.setItem("username", form.current.getFieldsValue().username)
        localStorage.setItem("password", form.current.getFieldsValue().password)
        setOpen(false);
        window.location.reload()
    };
    return (
        <div>

            {!isShowFooter ? (<div className='footer'>
                <div className='footer_empty'></div>
                <div className='footer_label'>
                    <h1>Don’t miss what’s happening</h1>
                    <h2>People on Twitter are the first to know.</h2>
                </div>
                <Button className='footer_btn' onClick={handleClick}>Log in</Button>
                <Modal title="Sign in to Twitter" open={open} onOk={handleOk} onCancel={handleClose}>
                    <Form
                        ref={form}
                        name="basic"
                        labelCol={{
                            span: 8,
                        }}
                        wrapperCol={{
                            span: 16,
                        }}
                        style={{
                            maxWidth: 600,
                        }}
                        initialValues={{
                            username: "demo",
                            password: "123456"
                        }}
                    >
                        <Form.Item
                            label="Username"
                            name="username"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your username!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Password"
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your password!',
                                },
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>
                    </Form>
                </Modal>

            </div>) : ''}
        </div>

    )
}

export default Footer
