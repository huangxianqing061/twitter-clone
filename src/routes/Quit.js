import React from 'react'
import { Avatar } from "@material-ui/core"
import "./Quit.css"
function Quit() {
    const handleQuit = () => {
        localStorage.clear()
        window.location.reload()
    }
  return (
    <div className='quit'>
      <div className='post_avatar' onClick={()=>(handleQuit())}>
        <Avatar src="" />
     </div>
    </div>
  )
}

export default Quit
