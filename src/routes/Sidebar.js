/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-03 21:24:54
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-07 22:29:57
 * @FilePath: /twitter/src/Sidebar.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React from "react";
import './Sidebar.css';
import TwitterIcon from "@material-ui/icons/Twitter"
import SidebarOption from "./SidebarOption";
import MobileSidebar from "./MobileSidebar";
import Quit from "./Quit";
import HomeIcon from "@material-ui/icons/Home"
import SearchIcon from "@material-ui/icons/Search"
import { Button } from "@material-ui/core";
import { useViewport } from '../util/ViewportContext';


function Sidebar() {
    const { width } = useViewport();
    const breakpoint = 760;
    const handleChange = () => {

    }
    return (
        width > breakpoint ? (<div className="sidebar">
        <TwitterIcon className="sidebar_twitterIcon"/>
        <SidebarOption active Icon={HomeIcon} text="Home" linkUrl="home"/>
        <SidebarOption Icon={SearchIcon} text="Explore" linkUrl="explore"/>
        {/* <div onClick={() => {handleChange()}}>changeColor</div> */}
        <Button variant="outlined" className="sidebar_tweet" fullWidth>Tweet</Button>
        <Quit />
    </div>) : <MobileSidebar />
        
    )
}
export default Sidebar; 