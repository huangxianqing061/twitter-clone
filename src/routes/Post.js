/*
 * @Author: huangxianqing06 huangxianqing06@126.com
 * @Date: 2023-02-05 19:04:28
 * @LastEditors: huangxianqing06 huangxianqing06@126.com
 * @LastEditTime: 2023-02-08 10:37:17
 * @FilePath: /twitter/src/Post.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React from 'react'
import "./Post.css"
import { Avatar } from "@material-ui/core"
import store from '../store/index.js'
import { Link } from "react-router-dom";
function Post({
  displayName,
  username,
  verified,
  text,
  image,
  avatar
}) {
  let [infoList, setInfoList] = React.useState(store.getState().infoList)
  store.subscribe(() => {
    setInfoList([...store.getState().infoList])
  })
  const handleDel = (id) => {
    const delData = {
      type: "DEL",
      id
    }
    store.dispatch(delData)
  }
  return (
    <div>
      <div className='tab_box'>
        <div className='tab_boxForYou'>For you</div>
        <div>Following</div>
      </div>
      {infoList.map((item, index) => {
        return (
          <div className='post_box'>
            <Link to={`/view?id=${item.id}`} style={{ textDecoration: 'none' }}>
              <div className='post'>
                <div className='post_avatar'>
                  <Avatar src="" />
                </div>
                <div className='post_body'>
                  <div className='post_header'>
                    <div className='post_headerText'>
                      <h3>
                        <span>{item.title}</span> <span className='time_box'>{item.time}</span>
                      </h3>
                    </div>
                    <div className='post_headerDes'>
                      <p>{item.content}</p>
                    </div>
                  </div>
                </div>
              </div>
            </Link>
            <div className='del_icon'>
              {item.userName === localStorage.getItem('username') ? (<div onClick={() => { handleDel(item.id) }}>...</div>) : ''}
            </div>
          </div>
        )
      })}

    </div>

  )
}

export default Post
